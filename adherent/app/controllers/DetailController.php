<?php
/**
 * Created by PhpStorm.
 * User: Romaric Mangin
 * Date: 11/4/2015
 * Time: 11:54 AM
 */
namespace app\controllers;
use app\models\Document as Document;

if (isset($_SESSION['id_doc'])) {
    $id = $_SESSION['id_doc'];
    unset($_SESSION['id_doc']);

    $doc = Document::find($id);
    if ($doc != null) {
        $doc->load('typedoc')
        ->load('etatemprunt')
        ->load('genre');
    }

    $_SESSION['document'] = $doc;
    unset($doc);
}