<?php

use app\models\Document;


$recherche = Document::where(function ($query) {
    $query->where("titre", "like", '%'.htmlspecialchars($_REQUEST['motcle'].'%'))
        ->orwhere("description", "like", "%".htmlspecialchars($_REQUEST['motcle']."%"));
});

if ( $_REQUEST['type'] != 0){
    $recherche->where(function ($query) {
        $query->where("idTypeDoc", "=", htmlspecialchars($_REQUEST['type']));
    });
}

if ($_REQUEST['genre'] != 0){
    $recherche->where(function ($query) {
        $query->where("idGenre","=",htmlspecialchars($_REQUEST['genre']));
    });
}

if ($_REQUEST['dispo'] != 0){
    $recherche->where(function ($query) {
        $query->where("idEtatEmprunt","=",htmlspecialchars($_REQUEST['dispo']));
    });
}

$recherche->get();
if ($recherche == null) {
    $_SESSION['research_to_affiche'] = null;
}
else
{
    $resultats = array();
    foreach ($recherche->get() as $doc) {
        array_push($resultats, array("titre" => $doc->titre, "description" => $doc->description, "id" => $doc->id, "photo" => $doc->photo));
    }
    $_SESSION['research_to_affiche'] = $resultats;
}




/*->where(function ($query) {
    $query->where("idTypeDoc", "=", htmlspecialchars($_REQUEST['type']))
        ->where("idGenre","=",htmlspecialchars($_REQUEST['genre']))
        ->where("idEtatEmprunt","=",htmlspecialchars($_REQUEST['dispo']));
})->get();;*/

//$_SESSION['research_to_affiche'] = $recherche;