<?php
/**
 * Created by PhpStorm.
 * User: Romaric Mangin
 * Date: 11/3/2015
 * Time: 10:46 AM
 */

use configuration\Connexion as Database;
use app\models\Menu as Menu;
use app\models\Genre as Genre;
use app\models\TypeDoc as TypeDoc;

Database::EloConfigure('database.ini');

$req = $app::getInstance()->request();
$uri = $req->getUrl() . $req->getRootUri();


// Homepage
$app->get('/', function() use ($app, $uri) {
    $menu = Menu::generateCollection();

    // Controller
    require_once('../app/controllers/accueilController.php');

    if (isset($_SESSION['listDoc'])) {
        $list = $_SESSION['listDoc'];
        unset($_SESSION['listDoc']);
    }
    else {
        $list = null;
    }


    $app->render('accueilAdherent.twig', array(
        'menuCollection' => $menu,
        "list" => $list,
        "url" => $uri,
        "page" => "accueil"));
});

// Connect
$app->get('/connect', function() use ($app, $uri) {
    $menu = Menu::generateCollection();

    $app->render(
        'connect.twig', array(
        'menuCollection' => $menu,
        "url" => $uri,
    ));
});

$app->post('/connect', function() use ($app, $uri) {
    $menu = Menu::generateCollection();
    require_once('../app/controllers/connectController.php');
    $app->render(
        'connect.twig', array(
        'menuCollection' => $menu,
        "url" => $uri,
        "info" => $_SESSION['user'],
        "succes" =>$_SESSION['co'],
        "nbtest" =>$_SESSION['nbtest'],
        "documents" =>$_SESSION['docs'],
    ));
});





// Detail Annonce
$app->get('/annonce/:id', function($id) use ($app, $uri) {
    $menu = Menu::generateCollection();

    // Controller
    $_SESSION['id_doc'] = $id;
    require_once('../app/controllers/DetailController.php');

    if (isset($_SESSION['document'])) {
        $doc = $_SESSION['document'];
        unset($_SESSION['document']);
    }
    else {
        $doc = null;
    }

    $app->render('detailAnnonceAdherent.twig', array("menuCollection" => $menu, "doc" => $doc,
        "url" => $uri));
});


// Recherche
$app->get('/recherche', function() use ($app, $uri) {
    $menu = Menu::generateCollection();
    $selectGenre = Genre::generateSelectAdd();
    $selectTypeDoc = TypeDoc::generateSelectAdd();

    $app->render('recherche.twig', array("menuCollection" => $menu, "url" => $uri,
            "genre" => $selectGenre, "type" => $selectTypeDoc)
    );
});

$app->post('/recherche', function() use ($app, $uri) {
    $menu = Menu::generateCollection();

    require('../app/controllers/recherche.php');

    $resultat = $_SESSION['research_to_affiche'];
    unset($_SESSION['research_to_affiche']);

    $app->render('resultat.twig', array("menuCollection" => $menu , "url" => $uri,
        'resultat' => $resultat, "page" => "accueil", "scroll" => "scroll"));
});


// If 404 not found
$app->notFound(function() use ($app, $uri) {
    $menu = Menu::generateCollection();

    $app->render('erreur.twig', array("menuCollection" => $menu, "url" => $uri));
});