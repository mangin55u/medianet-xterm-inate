<?php
/**
 * Created by PhpStorm.
 * User: Anthony
 * Date: 04/11/2015
 * Time: 10:29
 */

namespace app\models;
use Illuminate\Database\Eloquent\Model as Eloquent;


class TypeUtilisateur extends Eloquent {

    protected $table = 'typeutilisateur';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function utilisateurs(){
        return $this->hasMany('app\models\Utilisateur', "idTypeUtilisateur");
    }

}