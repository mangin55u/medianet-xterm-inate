<?php
/**
 * Created by PhpStorm.
 * User: Anthony
 * Date: 04/11/2015
 * Time: 10:29
 */

namespace app\models;
use Illuminate\Database\Eloquent\Model as Eloquent;


class EtatEmprunt extends Eloquent {

    protected $table = 'etatemprunt';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function documentsE(){
        return $this->hasMany('app\models\Document', "idEtatEmprunt");
    }

}