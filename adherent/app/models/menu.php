<?php

/**
 * Created by PhpStorm.
 * User: Anthony
 * Date: 03/11/2015
 * Time: 19:28
 */
namespace app\models;
use Illuminate\Database\Eloquent\Model as Eloquent;


class Menu extends Eloquent {
    protected $table = 'menu';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public static function generateCollection() {

        return (Menu::all());
    }
}


