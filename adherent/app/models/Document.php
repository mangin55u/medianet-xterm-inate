<?php
/**
 * Created by PhpStorm.
 * User: Anthony
 * Date: 04/11/2015
 * Time: 10:29
 */

namespace app\models;
use Illuminate\Database\Eloquent\Model as Eloquent;


class Document extends Eloquent {

    protected $table = 'document';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function etatEmprunt(){
        return $this->belongsTo('app\models\EtatEmprunt', "idEtatEmprunt");
    }

    public function genre(){
        return $this->belongsTo('app\models\Genre', "idGenre");
    }

    public function typedoc(){
        return $this->belongsTo('app\models\TypeDoc', "idTypeDoc");
    }

    public function utilisateur(){
        return $this->belongsTo('app\models\Utilisateur', "idUtilisateur");
    }

}