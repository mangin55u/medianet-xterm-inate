Membre du groupe: Anthony BARBIER, Romaric MANGIN, Adrien RICHARD, Corentin VUILLAUME


Lien application adhérent : https://webetu.iutnc.univ-lorraine.fr/~barbier51u/medianet/adherent/public/

Lien application staff : https://webetu.iutnc.univ-lorraine.fr/~barbier51u/medianet/staff/public/


###################
# Fonctionnalités #
###################

Adhérent : 

	- Liste documents récemment publiés (accueil)
	- Recherche par mot clé (titre, description), type, genre et disponibilité
	- Affichage des résultats de la recherche
	- Détail d'un document (avec disponibilité et date retour si emprunté)

	- Connexion d'un utilisateur et affichage des liste empruntés (avec date retour)

Staff : 

	- Emprunt 
	- Recapitulatif de l'emprunt (nombre de documents, liste des documents empruntés)
	- Retour des documents (Liste des documents rendus, fiche recapitulative pour chaque utilisateur dont les documents ont été rendu)
	- Recherche d'un client
	- Affichage du détail d'un client (documents emprunté avec date retour + infos client)
	
	- Ajouter un adhérent
	- Modifier un adhérent
	- Ajout type et genre de document
	- Supprimer et ajouter un document


INFORMATION:  - Pour se connecter en tant qu'adhérent, pour les utilisateurs de 1 à 4, le mot de passe est : voir readme.txt déposé dans l'archive.
		Sinon, vous pouvez créer un nouvel utilisateur depuis l'application staff.
	      - Si local, pensez à composer update et database.ini dans app/configuration/