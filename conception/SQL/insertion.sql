-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2015 at 11:19 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `medianet`
--

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `titre`, `description`, `photo`, `datesortie`, `dateEmprunt`, `idEtatEmprunt`, `idUtilisateur`, `idTypeDoc`, `idGenre`) VALUES
(1, 'Le parcours', 'C’est une minute après le grand séisme, le « Big One ». Marty Slack, un exécutif de chaîne de télévision, émerge de dessous sa Mercedes, garée devant ce qui fut un entrepôt au centre de Los Angeles et le lieu de tournage d’une nouvelle série télévisée. Le centre de Los Angeles est en ruines. Le ciel est recouvert d’une fumée noire. Le portable de Marty est mort. Les autoroutes sont sous les décombres. L’aéroport est démoli. Des bâtiments gisent dans les rues comme des arbres abattus. Ça prendra ', 'http://www.images-booknode.com/book_cover/656/le-parcours-655585-250-400.jpg', '2015-11-03', '2015-11-06', 1, 0, 3, 1),
(2, 'Hackeur et contre tous', 'Tout à coup, depuis ce matin, les cartes bancaires ne fonctionnent plus. L’argent liquide que vous avez sur vous, c’est tout ce qu’il vous reste. Les banques et les distributeurs de billets sont hors service. Vous ne pouvez pas acheter d’essence ni de nourriture. Tous les commerces sont fermés.\r\n\r\nNotre société est dépendante de l’ordinateur. Facebook. Twitter. LinkedIn. Nous sommes connectés. Nos comptes bancaires sont sur Internet. On télécharge des applis sur des iPhones, on lit sur des Kindl', 'http://ecx.images-amazon.com/images/I/91qTdqvr-uL.jpg', '2015-03-10', '2015-11-06', 1, 0, 3, 1),
(3, 'Justice Implacable', 'Le corps décapité d''une riche veuve est découvert à moitié décomposé dans la forêt de Chelling, suivi quelques jours plus tard d’une deuxième victime. L’inspecteur Lorne Simpkins et son collègue, le sergent Pete Childs, sont nommés sur l''affaire. Mais avant de pouvoir découvrir l''identité du tueur, ils doivent tout d’abord faire le lien entre les deux victimes.', 'http://ecx.images-amazon.com/images/I/61sCNfPnGCL.jpg', '2015-04-14', '0000-00-00', 1, 0, 3, 1),
(4, 'Retiens ton souffle', 'Un nouveau travail, une nouvelle maison, un nouvel homme… Samantha est bien décidée à se reconstruire après la tragédie familiale qui a bouleversé sa vie. Mais, lorsqu’un attentat secoue la ville de Washington, elle doit reprendre du service en tant que médecin légiste. Un gaz toxique a été libéré dans les tunnels du métro, et par miracle, seulement trois personnes sont mortes des suites de l’inhalation. Victimes du sort… ou d’un tueur diabolique ? Selon l’inspecteur Fletcher.', 'http://static.fnac-static.com/multimedia/Images/FR/NR/47/2a/72/7481927/1507-1.jpg', '2015-11-01', '0000-00-00', 1, 0, 3, 2),
(5, 'Le tueur est partout', 'Depuis sa greffe de cornée, Rachel de Luca, auteur à succès, voit enfin le monde tel qu’il est : chatoyant, saturé de formes et de couleurs, inondé de lumière... et parfois de ténèbres. Lorsque l’inspecteur Mason Brown vient l’avertir d’une série de meurtres, elle comprend que les problèmes sont de retour.', 'http://ecx.images-amazon.com/images/I/51mycr78sfL._AC_UL160_SR96,160_.jpg', '2015-11-01', '0000-00-00', 1, 0, 3, 1),
(6, 'Dans les yeux du tueur', 'Quarante-huit heures après la greffe de cornée dont elle a bénéficié, Rachel de Luca prend conscience qu’elle vit à la fois un miracle, et un cauchemar. Un miracle parce qu’elle a recouvré la vue alors qu’elle était aveugle depuis l’âge de douze ans.', 'http://www.editions-mosaic.fr/sites/default/files/images/livres/9782280319416-X_0.jpg', '2015-09-01', '0000-00-00', 1, 0, 3, 2),
(7, 'Les Chevaliers d''Emeraude', 'Exilé, Liam, le porteur de lumière, dépérit, comprenant qu il ne reverra plus jamais ses proches. Mais ces derniers ne l ont pas abandonné et font tout pour lui venir en aide par-delà les Territoires Inconnus. Acceptant alors enfin son destin, la princesse rebelle vole au secours du porteur de lumière et des Chevaliers d Émeraude qui subissent les attaques incessantes de l Empereur Noir. L ultime combat approche... Mais qui s en sortira vivant ?', 'http://static.fnac-static.com/multimedia/FR/Images_Produits/FR/fnac.com/Visual_Principal_340/7/5/3/9782749915357.jpg', '2012-01-13', '2015-11-05', 1, 0, 3, 3),
(8, 'Les Chevaliers D''Emeraude T2', 'Après des siècles de paix, les armées de l’Empereur Noir Amecareth envahissent soudain les royaumes du continent d’Enkidiev. Les Chevaliers d’Émeraude doivent alors protéger Kira, l’enfant magique liée à la prophétie et qui peut sauver le monde.\r\nComment ces monstres redoutables parviennent-ils à s’infiltrer sur le territoire d’Enkidiev sans être repérés par les Chevaliers d’Émeraude ? Au même moment, Asbeth, le sorcier de l’Empereur, s’apprête à enlever Kira…\r\nAfin de renforcer ses pouvoirs avant d’affronter ce redoutable homme-oiseau, le chef des Chevaliers, Wellan, se rend au Royaume des Ombres où il doit recevoir l’enseignement des Maîtres Magiciens. Là, il va découvrir un terrible secret…', 'http://img.over-blog-kiwi.com/0/88/81/93/20140410/ob_78aa5e_lees-chevaliers-d-emeraude-tome-2.jpg', '2012-04-05', '0000-00-00', 1, 0, 3, 3),
(9, 'Les chevaliers d''Emeraude T12', 'Exilé, Liam, le porteur de lumière, dépérit, comprenant qu il ne reverra plus jamais ses proches. Mais ces derniers ne l ont pas abandonné et font tout pour lui venir en aide par-delà les Territoires Inconnus. Acceptant alors enfin son destin, la princesse rebelle vole au secours du porteur de lumière et des Chevaliers d Émeraude qui subissent les attaques incessantes de l Empereur Noir. L ultime combat approche... Mais qui s en sortira vivant ?', 'http://wir.skyrock.net/wir/v1/resize/?c=isi&im=%2F3263%2F82083263%2Fpics%2F3101673931_1_9_e7FQwnfH.jpg&w=400', '2015-05-06', '2015-11-05', 1, 0, 3, 3),
(10, 'Le trône de fer, tome 1', 'Le royaume des sept couronnes est sur le point de connaître son plus terrible hiver : par-delà le mur qui garde sa frontière nord, une armée de ténèbres se lève, menaçant de tout détruire sur son passage. Mais il en faut plus pour refroidir les ardeurs des rois, des reines, des chevaliers et des renégats qui se disputent le trône de fer, tous les coups sont permis, et seuls les plus forts, ou les plus retors s''en sortiront indemnes...', 'http://www.ebook-in.com/wp-content/uploads/2013/12/tronedefer_tome1.jpg', '2015-01-20', '0000-00-00', 1, 0, 3, 3),
(11, 'Le Seigneur des Anneaux trilogie', 'Contient :\r\n- "Le Seigneur des Anneaux - La Communauté de l''Anneau"\r\n- "Le Seigneur des Anneaux - Les Deux Tours"\r\n- "Le Seigneur des Anneaux - Le retour du Roi"\r\nFilms en versions cinéma ', 'http://www.hdfever.fr/wp-content/uploads/2010/03/41y7gGCWbML._SS500_.jpg', '2014-04-11', '2015-11-05', 1, 0, 2, 3),
(12, 'Matrix - La trilogie ', 'Contient :\r\n- "Matrix"\r\n- "Matrix Reloaded"\r\n- "Matrix Revolutions" ', 'http://img5.leboncoin.fr/images/1d8/1d8903f100474a292c918149a0220a25ef85fb01.jpg', '2014-10-01', '0000-00-00', 1, 0, 2, 3),
(13, 'Underworld : L''intégrale', 'Contient :\r\n- "Underworld"\r\n- "Underworld 2 : Evolution"\r\n- "Underworld 3 : Le soulèvement des lycans"\r\n- Underworld : Nouvelle ère ', 'http://www.bluray-mania.com/img/p/4348-4473-large.jpg', '2012-06-08', '2015-11-05', 1, 0, 2, 19),
(14, 'Vice-versa ', 'Courts métrages :\r\n- "Lava" de James Ford Murphy (HD - 2014 - 7'')\r\n- "Premier rendez-vous ?" de Josh Cooley (HD - "Riley''s First Date?" - 2015)\r\nCommentaire audio de Pete Docter, Ronnie Del Carmen et Jonas Rivera (prod.)\r\n"Les chemins qui mènent à Pixar, ou la touche féminine de Vice-Versa"\r\n"Les émotions"', 'http://pixar-planet.fr/wp-content/uploads/2015/09/dvd-vice-versa.jpg', '2015-10-27', '2015-11-05', 1, 0, 2, 20),
(15, 'Mad Max : Fury Road', '"Furie maximum : le tournage de Fury Road" (29'')\r\n"Mad Max : la furie sur quatre roues" (23'')\r\n"Les Guerriers de la route : Max et Furiosa"\r\n"Les cinq épouses : Belles comme le jour"\r\n"Les outils du désert"\r\n"Fury Road : Accident et collision"\r\nScènes coupées', 'http://1.bp.blogspot.com/-W-muebln0kQ/Vc4XoDU3g3I/AAAAAAAAKBA/MP-5z3UWKq8/s1600/Mad%2BMax%2BRoad%2BFury%2BBlu-ray%2BCover%2BCaratula.jpg', '2015-10-14', '0000-00-00', 1, 0, 2, 2),
(16, 'Def Leppard', ' 1. Let''s Go\r\n  2. Dangerous\r\n  3. Man Enough\r\n  4. We Belong\r\n  5. Invincible\r\n  6. Sea of Love\r\n  7. Energized  \r\n  8. All Time High\r\n  9. Battle of My Own\r\n10. Broke ''n'' Brokenhearted\r\n11. Forever Young\r\n12. Last Dance\r\n13. Wings of an Angel\r\n14. Blind Faith\r\n\r\n', 'http://www.spoonradio.com/sites/default/files/acturock/focus2/NW_229-MA.jpg', '2015-10-30', '0000-00-00', 1, 0, 1, 12),
(17, 'Chronicles of the Immortals', 'Tracklist :\r\n\r\n1. Vision 11even * In My Universe\r\n2. Vision 12elve * Godmaker’s Temptation\r\n3. Vision 13teen * Stone Roses Edge\r\n4. Vision 14teen * Blood Of Eden (* All Love Must Die) [* The Rite] [* This Is The Night]\r\n5. Vision 15teen * Monster\r\n6. Vision 16teen * Diabolica Comedia\r\n7. Vision 17teen * Where Have The Children Gone\r\n8. Vision 18teen * The Last Fight\r\n9. Vision 19teen * Circle Of The Devil', 'http://www.vs-webzine.com/imgitw/vandenplas_g.jpg', '2015-11-06', '0000-00-00', 1, 0, 1, 12),
(18, 'Get Wet ', 'Get Wet est le premier album studio du groupe américain d''electronic dance music Krewella, initialement paru aux États-Unis le 20 septembre 2013, puis en France le 18 novembre 2013, signé chez Columbia Records. En France, l''album se place à la 100e place du classement, tandis qu''il se place à la 34e place aux États-Unis. En Nouvelle-Zélande, l''album se place 40e.', 'http://i.imgur.com/9mWA6Ks.png', '2013-11-18', '0000-00-00', 1, 0, 1, 12);

--
-- Dumping data for table `etatemprunt`
--

INSERT INTO `etatemprunt` (`id`, `intitule`) VALUES
(1, 'Disponible'),
(2, 'Indisponible'),
(3, 'Déjà emprunté');

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`id`, `intitule`) VALUES
(1, 'Policier'),
(2, 'Science-fiction'),
(3, 'Fantastique'),
(4, 'Historique'),
(5, 'BD'),
(6, 'Documentaire'),
(7, 'Manga'),
(8, 'Revue'),
(9, 'Jazz'),
(10, 'Hip-hop'),
(11, 'Pop'),
(12, 'Rock'),
(13, 'Electronique'),
(14, 'Classique'),
(15, 'Musique de film'),
(16, 'Reggae'),
(17, 'Comedie'),
(18, 'Drame'),
(19, 'Horreur'),
(20, 'Animation'),
(21, 'Western'),
(22, 'Serie'),
(23, 'Adulte');

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `intitule`, `ordre`, `url`) VALUES
(1, 'Accueil', 1, '/'),
(2, 'Recherche', 2, '/recherche');

--
-- Dumping data for table `typedoc`
--

INSERT INTO `typedoc` (`id`, `intitule`) VALUES
(1, 'CD'),
(2, 'DVD'),
(3, 'Livre');

--
-- Dumping data for table `typeutilisateur`
--

INSERT INTO `typeutilisateur` (`id`, `intitule`) VALUES
(1, 'client'),
(2, 'staff');

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `email`, `motpasse`, `nom`, `prenom`, `adresse`, `dateadhesion`, `idTypeUtilisateur`) VALUES
(1, 'romaric.mangin0947@gmail.com', 'dc76e9f0c0006e8f919e0c515c66dbba3982f785', 'Mangin55u', 'Romaric', '11 Rue Théodore Gouvy', '2015-11-06', 1),
(2, 'anthony.barbier5@gmail.com', '', 'Barbier', 'Anthony', '38 Bis Grande Rue', '2015-09-09', 1),
(3, 'adrien@adrien.fr', '', 'Richard', 'Adrien', '15 rue du Coquelicot', '2015-11-05', 1),
(4, 'coco@wiko.fr', '', 'Vuillaume', 'Corentin', '11 rue du Coq', '2015-11-04', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
