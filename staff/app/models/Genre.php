<?php
/**
 * Created by PhpStorm.
 * User: Anthony
 * Date: 04/11/2015
 * Time: 10:29
 */

namespace app\models;
use Illuminate\Database\Eloquent\Model as Eloquent;


class Genre extends Eloquent {

    protected $table = 'genre';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function documentsG(){
        return $this->hasMany('app\models\Document', "idGenre");
    }

}