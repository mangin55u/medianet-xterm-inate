<?php
/**
 * Created by PhpStorm.
 * User: Anthony
 * Date: 04/11/2015
 * Time: 10:29
 */

namespace app\models;
use Illuminate\Database\Eloquent\Model as Eloquent;


class Utilisateur extends Eloquent {

    protected $table = 'utilisateur';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function typeutilisateur(){
        return $this->belongsTo('app\models\TypeUtilisateur', "idTypeUtilisateur");
    }

    public function document(){
        return $this->hasMany('app\models\Document', "idUtilisateur");
    }

}