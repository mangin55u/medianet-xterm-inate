<?php

use app\models\TypeDoc as TypeDoc;
use app\models\Document as doc;


$titre = $_POST['titre'];
$description = $_POST['description'];
$image = $_POST['photo'];
$dateS = $_POST['datesortie'];
$dateE = date('0000-00-00');
$idEtatEmprunt = 1;
$idUtilisateur = 0;
$idType = $_POST['type'];
$idGenre = $_POST['genre'];

$newDoc = new doc();
$newDoc->titre=$titre;
$newDoc->description=$description;
$newDoc->photo=$image;
$newDoc->datesortie=$dateS;
$newDoc->dateEmprunt=$dateE;
$newDoc->idEtatEmprunt=$idEtatEmprunt;
$newDoc->idUtilisateur=$idUtilisateur;
$newDoc->idTypeDoc=$idType;
$newDoc->idGenre=$idGenre;
$newDoc->save();

if($newDoc != null) {
	$_SESSION['ajout'] = "ok";
}