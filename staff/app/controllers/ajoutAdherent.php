<?php

use app\models\Utilisateur as Utilisateur;

    $ajout = new Utilisateur();
    $ajout->nom = htmlspecialchars($_REQUEST['nom']);
    $ajout->prenom = htmlspecialchars($_REQUEST['prenom']);
    $ajout->adresse = htmlspecialchars($_REQUEST['adresse']);
    $ajout->dateadhesion = htmlspecialchars($_REQUEST['date']);
    $ajout->email = htmlspecialchars($_REQUEST['mail']);
    $ajout->motpasse = sha1($_REQUEST['mdp']);
    $ajout->idTypeUtilisateur = 1;
    $ajout->save();


$_SESSION['ajout_adherent'] = $ajout;