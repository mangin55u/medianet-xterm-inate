<?php

use app\models\TypeDoc as TypeDoc;


if(isset($_POST['typeAdd'])) {
	$intitule = TypeDoc::where("intitule", "like", "%".htmlspecialchars($_POST['typeAdd'])."%")->get();
	if (count($intitule) == 0) {
	 	$type=$_POST['typeAdd'];
		$_SESSION['type']=$type;
		$typeDoc = new TypeDoc();
		$typeDoc->intitule = strtoupper($type);
		$typeDoc->save();
	 }else{
	 	$_SESSION['typeBad'] = "erreur";
	 }
}