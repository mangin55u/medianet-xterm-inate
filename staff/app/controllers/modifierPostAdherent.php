<?php

use app\models\Utilisateur as Utilisateur;

$modif = Utilisateur::find($id);
$modif->nom = htmlspecialchars($_POST['nom']);
$modif->prenom = htmlspecialchars($_POST['prenom']);
$modif->adresse = htmlspecialchars($_POST['adresse']);
$modif->dateadhesion = htmlspecialchars($_POST['date']);
$modif->email = htmlspecialchars($_POST['mail']);
$modif->save();


$_SESSION['modif_adherent'] = $modif;