<?php

use app\models\Genre as Genre;


if(isset($_POST['genreAdd'])) {
	$intitule = Genre::where("intitule", "like", "%".htmlspecialchars($_POST['genreAdd'])."%")->get();
	if (count($intitule) == 0) {
	 	$genre=$_POST['genreAdd'];
		$_SESSION['genre']=$genre;
		$genreDoc = new Genre();
		$genreDoc->intitule = $genre;
		$genreDoc->save();
	 }else{
	 	$_SESSION['genreBad'] = "erreur";
	 }
}