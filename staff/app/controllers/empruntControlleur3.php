<?php


namespace app\controllers;
use app\models\Document as Document;

$idAdherent = $_SESSION['backup_cli'];
$docId = $_SESSION['idDoc'];
unset($_SESSION['idDoc']);

$doc = Document::find($docId);

if (($doc != null) && ($doc->idEtatEmprunt == 1)) {
	$tabAjout = $_SESSION['tabAjout'];
	array_push($tabAjout, $docId);
	$_SESSION['tabAjout'] = $tabAjout;
	$doc->idUtilisateur = $idAdherent;
	$doc->dateEmprunt = date('Y-m-d');
	$doc->idEtatEmprunt = 3;
	$doc->save();
}else{
	$tabErreur = $_SESSION['tabErreur'];
	array_push($tabErreur, $docId);
	$_SESSION['tabErreur'] = $tabErreur;
}