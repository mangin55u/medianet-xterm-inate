<?php

namespace configuration;

use Illuminate\Database\Capsule\Manager as Capsule;

class Connexion
{
    public static function EloConfigure($filename)
    {
        $config = parse_ini_file($filename);

        if (!$config)
            throw new Exception("App::eloConfigure: could not parse config file $filename <br />");

        $capsule = new Capsule();
        $capsule->addConnection($config);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
}