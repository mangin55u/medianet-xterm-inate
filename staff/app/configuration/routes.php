<?php

use configuration\Connexion as Database;

use app\models\Utilisateur as user;
use app\models\Document as doc;
use app\models\EtatEmprunt ;
use app\models\Menu as Menu;
use app\models\TypeDoc as Type;
use app\models\Genre as Genre;


Database::EloConfigure('database.ini');


$req = $app::getInstance()->request();
$uri = $req->getUrl() . $req->getRootUri();


// Homepage
$app->get('/', function() use ($app, $uri) {
    $app->render('homepage.twig', array("url" => $uri));
});

//Document options
$app->get('/document', function() use ($app, $uri) {
    $app->render('documentHomepage.twig', array("url" => $uri));
});

//adherent options
$app->get('/adherent', function() use ($app, $uri) {
    $app->render('adherentHomepage.twig', array("url" => $uri));
});

// ajout adhérent
$app->get('/adherent/ajout', function() use ($app, $uri) {
    $app->render('ajoutAdherent.twig', array("url" => $uri, "datepicker" => "oui"));
});
$app->post('/adherent/ajout', function() use ($app, $uri) {

    require('../app/controllers/ajoutAdherent.php');

    $ajout = $_SESSION['ajout_adherent'];
    unset($_SESSION['ajout_adherent']);

    $app->render('ajoutValide.twig', array("url" => $uri,
        'ajout' => $ajout));
});


// Modifier adhérent
$app->get('/adherent/modifier', function() use ($app, $uri) {

    $app->render('saisieAdherent.twig', array("url" => $uri, "datepicker" => "oui"));
});
$app->post('/adherent/modifier', function() use ($app, $uri) {
    $app->redirect($uri.'/adherent/modifierAdherent/'.$_POST['ref']);
});

$app->get('/adherent/modifierAdherent/:id', function($id) use ($app, $uri) {

    require('../app/controllers/modifierAdherentForm.php');
    $adherent = $_SESSION['recup_adherent'];

    if ($adherent ){
        unset($_SESSION['recup_adherent']);
        $app->render('modifierAdherent.twig', array("url" => $uri, "datepicker" => "oui",
            "adherent" => $adherent));
    }else{
        $app->redirect($uri.'/adherent/modifier');

    }
});
$app->post('/adherent/modifierAdherent/:id', function($id) use ($app, $uri){

    require('../app/controllers/modifierPostAdherent.php');
    $modif = $_SESSION['modif_adherent'];
    unset($_SESSION['modif_adherent']);

    $app->render('modificationValide.twig', array("url" => $uri,
        "modif" => $modif));

});


//Emprunt
$app->get('/emprunt', function() use ($app, $uri) {

    $app->render('empruntGet.twig', array("url" => $uri));
});

$app->post('/emprunt', function() use ($app, $uri) {

    if (!isset($_REQUEST['submit_emprunt'])) {
        if (isset($_REQUEST['number'])) {
            $user = user::find($_REQUEST['number']);

            if ($user == null) {
                $app->render('empruntGet.twig', array(
                "error" => "Cet user n'existe pas !",
                "url" => $uri));
            }
            else {
                require('../app/controllers/empruntControlleur.php');
                $_SESSION['backup_cli'] = $_SESSION['numClient'];
                unset($_SESSION['numClient']);

                if (isset($_SESSION['backup_cli'])) {
                    $numClient = $_SESSION['backup_cli'];
                    $_SESSION['tabAjout'] = array();
                    $_SESSION['tabErreur'] = array();

                    $app->render('empruntPost.twig', array(
                    "url" => $uri,
                    "nCl" => $numClient
                    ));
                }
            }
        }

        if (isset($_REQUEST['idref'])) {
            require('../app/controllers/empruntControlleur2.php');
            $idRef = $_SESSION['idRef'];
            unset($_SESSION['idRef']);
            $numClient = $_SESSION['backup_cli'];

            $doc = doc::find($idRef);
            $_SESSION['idDoc'] = $idRef;
            if ($doc == null) {
                require('../app/controllers/empruntControlleur3.php');
                $errorDoc = "Erreur";
            }
            else {
                require('../app/controllers/empruntControlleur3.php');
                $errorDoc = null;
            }

            $app->render('empruntPost.twig', array(
               "url" => $uri,
               "nCl" => $numClient,
               "errorDoc" => $errorDoc
            ));
        }
    }
    else {

        $liste = $_SESSION['tab_ref'];
        unset($_SESSION['tab_ref']);
        $docBon = $_SESSION['tabAjout'];
        unset($_SESSION['tabAjout']);
        $docErreur = $_SESSION['tabErreur'];
        unset($_SESSION['tabErreur']);

        $app->render('empruntRes.twig', array(
            "liste" => $liste,
            "listeEmprunt" => $docBon,
            "listeErreur" => $docErreur,
            "url" => $uri));
    }
});







// Client
$app->get('/client', function() use ($app, $uri) {

    $app->render('clientGet.twig', array("url" => $uri));
});

$app->post('/client', function() use ($app, $uri) {
    $refcli = $app->request->post('reference');
	$user = user::with('document')->find($refcli);
    if ($user == NULL) {
        $app->render('clientGet.twig',array(
            "url" => $uri,
            "trouve" => 'nope'
        ));
    } else {
        $app->render('clientPost.twig',array(
        	"url" => $uri,
            "user" => $user
        ));
    }

});

// Ajout type de document

$app->get('/document/ajoutType', function() use ($app, $uri) {
    $app->render('ajoutTypeDoc.twig', array("url" => $uri));
});

$app->post('/document/ajoutType', function() use ($app, $uri) {
    if(isset($_POST['envoyer'])) {
        require('../app/controllers/addType.php');
        if (isset($_SESSION['type'])) {
            $typeBon = $_SESSION['type'];
            unset($_SESSION['type']);
            $app->render('ajoutTypeDoc.twig', array(
                "url" => $uri,
                "type" => $typeBon
            ));
        }else {
            $typeBad = $_SESSION['typeBad'];
            unset($_SESSION['typeBad']);
            $app->render('ajoutTypeDoc.twig', array(
                "url" => $uri,
                "typeBad" => $typeBad
            ));
        }
    }
});


// Ajout genre de document

$app->get('/document/ajoutGenre', function() use ($app, $uri) {
    $app->render('ajoutGenreDoc.twig', array("url" => $uri));
});

$app->post('/document/ajoutGenre', function() use ($app, $uri) {
    if(isset($_POST['envoyer'])) {
        require('../app/controllers/addGenre.php');
        if (isset($_SESSION['genre'])) {
            $genreBon = $_SESSION['genre'];
            unset($_SESSION['genre']);
            $app->render('ajoutGenreDoc.twig', array(
                "url" => $uri,
                "genre" => $genreBon
            ));
        }else {
            $genreBad = $_SESSION['genreBad'];
            unset($_SESSION['genreBad']);
            $app->render('ajoutGenreDoc.twig', array(
                "url" => $uri,
                "genreBad" => $genreBad
            ));
        }
    }
});




// Supprimer un document

$app->get('/document/supprimer', function() use ($app, $uri) {

    $titres=doc::all();
    $app->render('deleteDoc.twig', array(
        "url" => $uri,
        "ltitre" => $titres
    ));
});

$app->post('/document/supprimer', function() use ($app, $uri) {
    if(isset($_POST['supprimer'])) {
        require('../app/controllers/deleteDoc.php');

        $titres=doc::all();
        $titre = $_SESSION['titre'];
        $app->render('deleteDoc.twig', array(
            "url" => $uri,
            "ltitre" => $titres,
            "titre" => $titre
        ));
    }
});


// Ajouter un document

$app->get('/document/ajout', function() use ($app, $uri) {

    $allType=Type::all();
    $allGenre=Genre::all();

    $app->render('addDoc.twig', array(
        "url" => $uri,
        "lType" => $allType,
        "lGenre" => $allGenre,
        "datepicker" => "oui"
    ));
});

$app->post('/document/ajout', function() use ($app, $uri) {
    if(isset($_POST['ajouter'])) {
        require('../app/controllers/addDoc.php');

        $allType=Type::all();
        $allGenre=Genre::all();
        $msg = $_SESSION['ajout'];
        unset($_SESSION['ajout']);

        $app->render('addDoc.twig', array(
            "url" => $uri,
            "lType" => $allType,
            "lGenre" => $allGenre,
            "msg" => $msg
        ));
    }
});


//Remise


$app->get('/remise', function() use ($app, $uri) {
    $app->render('retourDocGet.twig', array(
        "url" => $uri
    ));
});



if (!isset($_SESSION['refs']) ) {
    $_SESSION['refs']=array();
}

$app->post('/remise', function() use ($app, $uri) {



    if (! isset($_REQUEST['finRendre']) ) {
        // Liste des documents
        array_push($_SESSION['refs'], $app->request->post('refdoc') );
        $app->render('retourDocGet.twig', array(
            "url" => $uri
        ));
    } else {
        $app->redirect($uri.'/recap-retour') ;
    }


});



$app->get('/recap-retour', function() use ($app, $uri) {
    $lref = $_SESSION['refs'];

    $luser=array();
    $lid=array();

    $lerreur=array();
    $lrendre=array();

    foreach ($lref as $key => $ref) {
        $doc =doc::with('utilisateur')->find($ref);

        if ($doc && $doc->etatEmprunt->id == '3' ) {
            array_push($lrendre, $doc);

            $u = user::find($doc->utilisateur->id);
            $u->load('document');

            $doc->idEtatEmprunt=1;
            $doc->dateEmprunt='0000-00-00';
            $doc->idUtilisateur = 0;
            $doc->save();

            if (!in_array($u->id, $lid)) {
                array_push($lid, $u->id );
            }
        }else {
            array_push($lerreur, $ref);
        }
    }
    foreach ($lid as $key => $id) {
        $user= user::with('document')->find($id);
        array_push($luser, $user);

    }


    unset($_SESSION['refs']);

    $app->render('recapRetour.twig', array(
            "url" => $uri,
            "erreurs" => $lerreur,
            "rendus" => $lrendre,
            "luser" => $luser,
        ));


});

// If 404 not found
$app->notFound(function() use ($app, $uri) {

    $app->render('erreur.twig', array("url" => $uri));
});