/**
 * Created by Anthony on 06/11/2015.
 */
$(document).ready(function() {
    $('#datepicker').pickadate({
        format: 'dddd dd mmmm yyyy',
        formatSubmit: 'yyyy-mm-dd',

        monthsFull: [ 'Janvier', 'F�vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao�t', 'Septembre',
            'Octobre', 'Novembre', 'D�cembre' ],
        monthsShort: [ 'Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec' ],
        weekdaysFull: [ 'Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi' ],
        weekdaysShort: [ 'Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam' ],
        today: 'Aujourd\'hui',
        clear: 'Effacer',
        close: 'Fermer',
        firstDay: 1,
        labelMonthNext:"Mois suivant",
        labelMonthPrev:"Mois pr�c�dent",
        labelMonthSelect:"S�lectionner un mois",
        labelYearSelect:"S�lectionner une ann�e",

        hiddenName: true
    });
});